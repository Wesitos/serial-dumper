import serial
import os
from os import path
from arrow import Arrow

conn = serial.Serial()
conn.baudrate= '9600'

conn.port = 'COM3'

filename_date_format = 'YY-MM-DD HH_mm_ss'
date_format = 'DD/MM/YY HH:mm:ss'

out_dir = path.join(path.dirname(__file__) , 'out')

def main():
    with conn as ser:
        filename = Arrow.now().format(filename_date_format) + '.csv'
        with open(path.join(out_dir,  filename), 'a') as f:
            line = ""
            while True:
                char = ser.read().decode('utf-8')
                print(char, end='', flush=True)
                line = line + char
                if char == "\n" and line.strip():
                    date = Arrow.now().format(date_format)
                    line = date + '\t' + line
                    f.write(line.strip().replace('\t', ',') + '\n')
                    f.flush()
                    line = ''

if __name__ == '__main__':
    try:
        os.mkdir(out_dir)
    except FileExistsError:
        pass

    main()
